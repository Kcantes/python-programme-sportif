import Exercises from '../views/Exercises.vue';
import nProgress from 'nprogress';
import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Session from '../views/Session.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Session',
    component: Session
  },
  {
    path: '/exercises',
    name: 'Exercises',
    component: Exercises
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});
router.beforeEach(async (to, from, next) => {
  document.title = 'Sports Web App';
  nProgress.start();
  next();
});
router.afterEach(() => {
  nProgress.done();
});

export default router;
