export interface Exercise {
  label: string;
  iterations: number;
  duration: number;
  pause: number;
  categories: string[];
}
