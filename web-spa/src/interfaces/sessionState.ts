export enum sessionState {
    initial,
    exercising,
    pausing,
    sessionPaused,
    sessionEnded,
    interlude
}