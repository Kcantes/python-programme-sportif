export class Timer {
    timer = 0;
    private isRunning = false;

    private sleep(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    async startTimer(seconds: number) {
        this.isRunning = true;
        while (this.timer < seconds) {
            await this.sleep(1000);
            if (this.isRunning) {
                this.timer++;
            }
        }
        this.resetTimer();
    }

    pauseTimer() {
        this.isRunning = false;
    }
    unpauseTimer() {
        this.isRunning = true;
    }
    resetTimer() {
        this.timer = 0;
    }

}