import Vue from 'vue';
import Vuex from 'vuex';
import { Exercise } from '@/interfaces/exercise';
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    exercises: [
      { label: 'Push ups', iterations: 2, duration: 30, pause: 30, categories: ["triceps", "pecs"] },
      { label: 'Diamond push ups', iterations: 2, duration: 30, pause: 30, categories: ["pecs"] },
      { label: 'Dips with chair', iterations: 2, duration: 30, pause: 30, categories: ["triceps", "shoulders"] },
      // {
      //   label: 'Shoulders upwards',
      //   iterations: 2,
      //   duration: 30,
      //   pause: 30
      // },
      // { label: 'Biceps curl', iterations: 2, duration: 30, pause: 30 },
      { label: 'Squats', iterations: 2, duration: 60, pause: 30, categories: ["legs"] },
      { label: 'Burpees with push ups', iterations: 2, duration: 60, pause: 45, categories: ["triceps", "pecs", "legs"] },
      { label: 'Lunges', iterations: 2, duration: 60, pause: 30, categories: ["legs"] },
      { label: 'Sit ups', iterations: 2, duration: 45, pause: 30, categories: ["abdos"] },
      { label: 'Gainage', iterations: 2, duration: 45, pause: 30, categories: ["abdos"] },
      { label: 'Mountain climber', iterations: 2, duration: 30, pause: 30, categories: ["abdos", "legs"] },
      { label: 'Toetouch Crunch', iterations: 2, duration: 45, pause: 30, categories: ["abdos", "legs"] },
      { label: 'Archer Push ups', iterations: 2, duration: 30, pause: 30, categories: ["triceps"] },
      { label: 'Flexion', iterations: 2, duration: 30, pause: 30, categories: ["legs"] },
      { label: 'Dumbells biceps', iterations: 2, duration: 30, pause: 30, categories: ["biceps"] },
      { label: 'Dumbells back', iterations: 2, duration: 30, pause: 30, categories: ["back"] }
    ] as Exercise[]
  },
  mutations: {},
  actions: {},
  modules: {}
});
