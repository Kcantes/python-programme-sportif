import time
import random as rd
from playsound import playsound
from gtts import gTTS
import os
# VARS-------------------------------------------------------------
exos_baseIt_baseNumberSeries_baseTime_basePause = [
    ("Push ups", 2, 30, 30),
    ("Diamond push ups", 2, 30, 30),
    ("Dips with chair", 2, 30, 30),
    ("Shoulders upwards", 2, 30, 30),
    ("Biceps curl", 2, 45, 30),
    ("Squats", 2, 60, 30),
    ("Burpees with push ups", 2, 60, 45),
    ("Lunges", 2, 60, 30),
    ("Sit ups", 2, 45, 30),
    ("Gainage", 2, 45, 30),
    ("Mountain climber", 2, 30, 30),
    ("Toetouch Crunch", 2, 45, 30),
    ("Archer Push ups", 2, 30, 30)
]
print("Session Time (30min): ")
try:
    t = int(input())
except:
    t = 30

exos = exos_baseIt_baseNumberSeries_baseTime_basePause.copy()
dfficulty = 1.6
exo_time = t

c_dir = os.getcwd()
audio_path = "/audio/"
# FUNCTION---------------------------------------


def play(txt):
    if(os.path.exists(c_dir+audio_path+txt+".mp3")):
        playsound(c_dir+audio_path+txt+".mp3")
    else:
        myobj = gTTS(text=txt, lang="en", slow=False)
        myobj.save(c_dir+audio_path + txt + ".mp3")
        play(txt)


t = time.time()
while ((time.time()-t)/60 < exo_time):
    try:
        print(int((time.time()-t)/60), "min")
        play(str(int((time.time()-t)/60)) + " min")
        rdm = rd.randint(0, len(exos)-1)
        print(exos[rdm][0]+" : " + str(exos[rdm][2]) + " seconds")
        play(exos[rdm][0])
        time.sleep(3)
        for k in range(exos[rdm][1]):
            print("START " + str(exos[rdm][2]) + " seconds of " + exos[rdm][0])
            play("START " + str(exos[rdm][2]) + " seconds of " + exos[rdm][0])
            t1 = time.time()
            while(time.time()-t1 < exos[rdm][2]):
                print(exos[rdm][0] + " " +
                      str(exos[rdm][2] - int(time.time()-t1)) + "s", end="\r", flush=True)
                time.sleep(1)
            pause = int(exos[rdm][3] / dfficulty)
            play("STOP")
            play("PAUSE " + str(pause) + " seconds")
            t1 = time.time()
            while(time.time()-t1 < pause):
                print("PAUSE" + " " +
                      str(pause-int(time.time()-t1)) + "s", end="\r", flush=True)
                time.sleep(1)
            play("END OF PAUSE")
    except Exception as e:
        print(e)

play("END OF SESSION")
while(1):
    time.sleep(1)
